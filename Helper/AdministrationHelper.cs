﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using AdminPanel.DataBase;
using AdminPanel.Models;
using AdminPanel.Models.Login;

namespace AdminPanel.Helper
{
    public static class AdministrationHelper
    {
        public static bool ExecutiveLogin(string userName, string password, ref string msg)
        {
            bool issuccess = false;
            try
            {
                if (ConnectToDatabase.CheckIPAddressForLogin(userName, UtilityClass.GetLocalIPAddress()))
                {
                    DataTable dtExecutive = ConnectToDatabase.UserLogin_PP(userName, password);

                    if (dtExecutive != null && dtExecutive.Rows.Count > 0)
                    {
                        issuccess = true;
                        InitializeAgencySession(dtExecutive);
                    }
                    else
                    {
                        msg = "Userid/Password seems to be incorrect, Please try again?";
                    }
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return issuccess;
        }
        private static void InitializeAgencySession(DataTable dtExecutive)
        {
            if (dtExecutive != null && dtExecutive.Rows.Count > 0)
            {
                ExecuRegister olu = new ExecuRegister();

                olu.counter = !string.IsNullOrEmpty(dtExecutive.Rows[0]["counter"].ToString()) ? Convert.ToInt32(dtExecutive.Rows[0]["counter"].ToString()) : 0;
                olu.user_id = !string.IsNullOrEmpty(dtExecutive.Rows[0]["user_id"].ToString()) ? dtExecutive.Rows[0]["user_id"].ToString() : string.Empty;
                olu.password = !string.IsNullOrEmpty(dtExecutive.Rows[0]["password"].ToString()) ? dtExecutive.Rows[0]["password"].ToString() : string.Empty;
                olu.role_id = !string.IsNullOrEmpty(dtExecutive.Rows[0]["role_id"].ToString()) ? Convert.ToInt32(dtExecutive.Rows[0]["role_id"].ToString()) : 0;
                olu.role_type = !string.IsNullOrEmpty(dtExecutive.Rows[0]["role_type"].ToString()) ? dtExecutive.Rows[0]["role_type"].ToString() : string.Empty;
                olu.name = !string.IsNullOrEmpty(dtExecutive.Rows[0]["name"].ToString()) ? dtExecutive.Rows[0]["name"].ToString() : string.Empty;
                olu.email_id = !string.IsNullOrEmpty(dtExecutive.Rows[0]["email_id"].ToString()) ? dtExecutive.Rows[0]["email_id"].ToString() : string.Empty;
                olu.mobile_no = !string.IsNullOrEmpty(dtExecutive.Rows[0]["mobile_no"].ToString()) ? dtExecutive.Rows[0]["mobile_no"].ToString() : string.Empty;
                olu.status = !string.IsNullOrEmpty(dtExecutive.Rows[0]["status"].ToString()) ? Convert.ToBoolean(dtExecutive.Rows[0]["status"].ToString()) : false;
                olu.trip = !string.IsNullOrEmpty(dtExecutive.Rows[0]["trip"].ToString()) ? dtExecutive.Rows[0]["trip"].ToString() : string.Empty;
                olu.Branch = !string.IsNullOrEmpty(dtExecutive.Rows[0]["Branch"].ToString()) ? dtExecutive.Rows[0]["Branch"].ToString() : string.Empty;

                List<ExecuRegister> lu = new List<ExecuRegister>();
                lu.Add(olu);
                HttpContext.Current.Session["executive"] = lu;
            }
        }
        public static bool IsExecutiveLogin(ref ExecuRegister lu)
        {
            List<ExecuRegister> objLoginUser = new List<ExecuRegister>();
            bool idsuccess = IsExecutiveLoginSuccess(ref objLoginUser);
            if (objLoginUser != null && objLoginUser.Count > 0)
            {
                lu = objLoginUser[0];
            }
            return idsuccess;
        }
        public static bool IsExecutiveLoginSuccess(ref List<ExecuRegister> loginUserList)
        {
            bool value = false;
            try
            {
                if (HttpContext.Current.Session["executive"] != null)
                {
                    loginUserList = (List<ExecuRegister>)HttpContext.Current.Session["executive"];
                    value = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return value;
        }
        public static bool LogoutExecutive()
        {
            HttpContext.Current.Session.Remove("executive");
            return true;
        }
    }
}