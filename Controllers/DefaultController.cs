﻿using AdminPanel.Models.Login;
using AdminPanel.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminPanel.Controllers
{
    public class DefaultController : Controller
    {
        #region[Admin-Login]
        public ActionResult ExecutiveLogin()
        {
            return View();
        }

        public JsonResult ProcessBackendLogin(string username, string password)
        {
            string msg = string.Empty;
            return Json(AdministrationService.ExecutiveLogin(username, password, ref msg));
        }
        #endregion
    }
}