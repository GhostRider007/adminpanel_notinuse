﻿$("#btnBackLogin").click(function () {


    var thisbutton = $("#btnBackLogin");
    var username = $("#txtusername").val();
    var password = $("#txtpassword").val();
    var isrember = null;

    if ($("#ckbRemeber").is(':checked')) { isrember = "true"; }
    else { isrember = "false"; }

    if (username == null || username == "") { $(".usernamesec").css("border-bottom", "1px solid red"); $("#txtusername").focus(); return false; } else { $(".usernamesec").css("border-bottom", "1px solid #b2b2b2"); }
    if (username != "" && (password == null || password == "")) { $(".userpasssec").css("border-bottom", "1px solid red"); $("#txtpassword").focus(); return false; } else { $(".userpasssec").css("border-bottom", "1px solid #b2b2b2"); }

    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner processspin'></i>");

    $.ajax({
        type: "Post",
        url: "/Default/ProcessBackendLogin",
        data: '{username: ' + JSON.stringify(username) + ',password: ' + JSON.stringify(password) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    localStorage.setItem("backloginid", data[1]);
                    window.location.href = "/backend-dashboard";
                }
                else {
                    $(".errormsg").removeClass("hidden").html(data[1]);
                    localStorage.removeItem("backloginid");
                    $(thisbutton).html("Login");
                }
            }
        }
    });
});
