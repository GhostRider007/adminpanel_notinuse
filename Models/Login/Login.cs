﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminPanel.Models.Login
{
    public class ExecuRegister
    {
        public int counter { get; set; }
        public string user_id { get; set; }
        public string password { get; set; }
        public int role_id { get; set; }
        public string role_type { get; set; }
        public string name { get; set; }
        public string email_id { get; set; }
        public string mobile_no { get; set; }
        public bool status { get; set; }
        public string trip { get; set; }
        public string Branch { get; set; }
    }
}