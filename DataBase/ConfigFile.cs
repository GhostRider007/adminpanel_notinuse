﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace AdminPanel.DataBase
{
    public static class ConfigFile
    {
        public static string DatabaseConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["dtconnection"].ConnectionString; }
        }
    }
}