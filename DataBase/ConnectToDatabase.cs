﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AdminPanel.DataBase
{
    public static class ConnectToDatabase
    {
        #region [Common : Connect to database]                
        private static SqlConnection conString = new SqlConnection(ConfigFile.DatabaseConnectionString);
        private static SqlCommand Command { get; set; }
        private static SqlDataAdapter Adapter { get; set; }
        private static DataSet ObjDataSet { get; set; }
        private static DataTable ObjDataTable { get; set; }
        private static void OpenConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Closed)
            {
                constr.Open();
            }
        }
        public static void CloseConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Open)
            {
                constr.Close();
            }
        }
        #endregion

        #region [Fetch Data Records]        
        public static bool CheckIPAddressForLogin(string userId, string ipAddress)
        {
            bool issuccess = false;
            try
            {
                conString.Open();
                Command = new SqlCommand("CheckLoginIpAddress", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("UserId", userId);
                Command.Parameters.AddWithValue("IPAddress", ipAddress);

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "LoginIpAddress");
                ObjDataTable = ObjDataSet.Tables["LoginIpAddress"];
                conString.Close();

                if (ObjDataTable != null && ObjDataTable.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ObjDataTable.Rows[0]["IPCount"])) && Convert.ToInt32(ObjDataTable.Rows[0]["IPCount"]) > 0 && Convert.ToInt32(ObjDataTable.Rows[0]["RCount"]) > 0)
                    {
                        issuccess = true;
                    }
                    else
                    {
                        issuccess = false;
                    }
                }
                else
                {
                    issuccess = false;
                }
            }
            catch (Exception ex)
            {
                if (conString.State != ConnectionState.Closed)
                {
                    conString.Close();
                }
            }

            return issuccess;
        }
        public static DataTable UserLogin_PP(string userName, string password)
        {
            try
            {
                conString.Open();
                Command = new SqlCommand("UserLogin_PP", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("Usename", userName);
                Command.Parameters.AddWithValue("Password", password);

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "UserLogin");
                ObjDataTable = ObjDataSet.Tables["UserLogin"];
                conString.Close();
            }
            catch (Exception ex)
            {
                if (conString.State != ConnectionState.Closed)
                {
                    conString.Close();
                }
            }

            return ObjDataTable;
        }
        #endregion
    }
}