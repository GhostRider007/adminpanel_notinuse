﻿using AdminPanel.Helper;
using AdminPanel.Models.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminPanel.Service
{
    public static class AdministrationService
    {
        public static List<string> ExecutiveLogin(string userName, string password, ref string msg)
        {
            List<string> result = new List<string>();
            try
            {
                bool isLogin = AdministrationHelper.ExecutiveLogin(userName, password, ref msg);
                if (isLogin)
                {
                    result.Add("true");
                }
                else
                {
                    result.Add("false");
                    result.Add(msg);
                }
            }
            catch (Exception ex)
            {
                result.Add("false");
                result.Add(ex.Message);
            }
            return result;
        }
    }
}